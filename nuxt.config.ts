export default defineNuxtConfig({
    css: ['~/assets/css/main.css','vuetify/lib/styles/main.sass'],
    build: {
        transpile: ['vuetify'],
    },
    vite: {
        define: {
            'process.env.DEBUG': false,
        },
    }
})